export {ReactComponent as CheckIcon} from './check.svg'; 
export {ReactComponent as CloseIcon} from './close.svg'; 
export {ReactComponent as ResearchIcon} from './research.svg'; 
export {ReactComponent as TrashIcon} from './trash.svg'; 
export {ReactComponent as EyeIcon} from './eye.svg'; 
export {ReactComponent as EditIcon} from './edit.svg'; 
export {ReactComponent as LogoWhite} from './logowhite.svg'; 
