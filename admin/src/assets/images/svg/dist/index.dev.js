"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CheckIcon", {
  enumerable: true,
  get: function get() {
    return _check.ReactComponent;
  }
});
Object.defineProperty(exports, "CloseIcon", {
  enumerable: true,
  get: function get() {
    return _close.ReactComponent;
  }
});
Object.defineProperty(exports, "ResearchIcon", {
  enumerable: true,
  get: function get() {
    return _research.ReactComponent;
  }
});
Object.defineProperty(exports, "TrashIcon", {
  enumerable: true,
  get: function get() {
    return _trash.ReactComponent;
  }
});
Object.defineProperty(exports, "EyeIcon", {
  enumerable: true,
  get: function get() {
    return _eye.ReactComponent;
  }
});
Object.defineProperty(exports, "EditIcon", {
  enumerable: true,
  get: function get() {
    return _edit.ReactComponent;
  }
});

var _check = require("./check.svg");

var _close = require("./close.svg");

var _research = require("./research.svg");

var _trash = require("./trash.svg");

var _eye = require("./eye.svg");

var _edit = require("./edit.svg");