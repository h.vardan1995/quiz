export const certificateConfig = {
  ARMENIAN_TEXT: {
    regexp: '{level_text_armenian}',
    text: '«ՀԱՄԱՅՆՔԻ ԱԿՏԻՎ ԲՆԱԿԻՉ»'
  },
  ENGLISH_TEXT: {
    regexp: '{level_text_english}',
    text: '“ACTIVE COMMUNITY RESIDENT”'
  }
};
