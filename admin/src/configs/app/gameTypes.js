export const GAME_TYPES = {
  ACTIVE_RESIDENT_OF_THE_COMMUNITY: {
    id: 'ActiveResidentOfTheCommunity',
    name: 'Համայնքի ակտիվ բնակիչ'
  },
  CANDIDATE_FOR_THE_COUNCIL_OF_ELDERS: {
    id: 'CandidateForTheCouncilOfElders',
    name: 'Համայնքի ավագանու անդամ'
  }
}