import apiService from "../../api";

class QuestionUpdateController {
  async questionAdd(questionData) {
    try {
      const questionResponse = await apiService.post(
        "/Questions/UpdateQuestion",
        questionData
      );

      return questionResponse.data;
    } catch (error) {
      const errorInfo = error.response.data;

      return Promise.reject(errorInfo);
    }
  }
}

const questionUpdateController = new QuestionUpdateController();

export default questionUpdateController;
