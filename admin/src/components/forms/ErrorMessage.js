import React from "react";

const ErrorMessage = ({ children }) => {
  if (typeof children !== "string") return null;

  return <p className="error-massage mt-2">{children}</p>;
};

export default ErrorMessage;
