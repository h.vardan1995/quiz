import React, { Component, Suspense, lazy } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Spinner from "./shared/Spinner";
import Questions from "../components/questions/Questions";
import Users from "../components/users/Users";
import WrongUsers from "../components/wrong-users/WrongUsers";
import ResetPassword from "../components/reset-password";
import Login from "../components/login";
import QuestionsList from "./questions/QuestionsList";
import Certificate from "./certificate/Certificate";

class AppRoutes extends Component {
  render() {
    const windowLocation = window.location.pathname;
    console.log("win]]", windowLocation);
    return (
      <Suspense fallback={<Spinner />}>
        <div
          className={windowLocation === "/certificate" ? "" : "page__wrapper"}
        >
          <Switch>
            <Route exact path="/questions" component={QuestionsList} />
            <Route exact path="/questions/add" component={Questions} />
            <Route
              exact
              path="/questions/edit/:questionId"
              render={(props) => <Questions {...props} isEdit />}
            />

            <Route exact path="/users" component={Users} />
            <Route
              exact
              path="/wrong-answers/:playerId"
              component={WrongUsers}
            />
            <Route exact path="/login" component={Login} />
            <Route exact path="/reset-password" component={ResetPassword} />
            <Route exact path="/certificate" component={Certificate} />

            <Redirect to="/questions" />
          </Switch>
        </div>
      </Suspense>
    );
  }
}

export default AppRoutes;
