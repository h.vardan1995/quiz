import React, { useCallback, useEffect, useState } from 'react';
import { certificateConfig } from '../../configs/app/certificateConfig';
import { useCertificationStore } from '../../store/certification';
import { CustomButton } from '../shared';
import UnchangeableInput from './../shared/UnchangeableInput';
import classes from './Certification.module.scss';

const Certificate = () => {
  const { certification, certificationGet, certificationEdit } =
    useCertificationStore();

  const [enText, setEnText] = useState('');
  const [amText, setAmText] = useState('');

  const onEditClick = useCallback(() => {
    if (!enText && !amText) return;

    certificationEdit(
      enText || certification?.englishText,
      amText || certification?.armenianText
    );
  }, [enText, amText]);

  useEffect(() => {
    certificationGet();
  }, [certificationGet]);

  return (
    <div className={classes.Certificate}>
      <div className={classes.CertificateWrapper}>
        <div className={classes.certificateTextWrapper}>
          <p className={classes.title}>Հավաստագիր </p>
          <p className={classes.titleEn}>CERTIFICATE OF ACHIEVEMENT</p>
          <p className={classes.presented}>Տրվում է / Presented to</p>
          <p className={classes.name}>Անուն Ազգանուն</p>
          <div className={classes.aboutText}>
            <UnchangeableInput
              defaultValue={certification?.armenianText}
              regexp={certificateConfig.ARMENIAN_TEXT.regexp}
              disabledText={certificateConfig.ARMENIAN_TEXT.text}
              onChange={setAmText}
              className={classes.about}
            />

            <UnchangeableInput
              defaultValue={certification?.englishText}
              regexp={certificateConfig.ENGLISH_TEXT.regexp}
              disabledText={certificateConfig.ENGLISH_TEXT.text}
              onChange={setEnText}
              className={classes.aboutEn}
            />
          </div>
          <div className={classes.signatureWrapper}>
            <div className={classes.signature}></div>
            <p className={classes.date}>24․05․2021</p>
            <p className={classes.place}>Լոռու մարզ, Ստեփանավան համայնք</p>
          </div>

          {/* <Medal_2 className={classes.certificationMedal} /> */}
        </div>
      </div>

      <div className={classes.ButtonWrapper}>
        <CustomButton
          buttonTitle={'Փոփոխել'}
          className='btn btn-dark'
          buttonIcon={'fas fa-check mr-2'}
          type='button'
          handleButtonClick={onEditClick}
        />
      </div>
    </div>
  );
};

export default Certificate;
