import React, { useState, useEffect } from 'react';
import {
  ResearchIcon,
  CheckIcon,
  CloseIcon,
  TrashIcon
} from '../../assets/images/svg';
import cogoToast from 'cogo-toast';
import SweetAlert from 'sweetalert2-react';
import CustomTable from '../shared/CustomTable';
import { usePlayersStore } from '../../store/players';
import CustomPopup from '../shared/CustomPopup';
import { Link, useHistory } from 'react-router-dom';
import CustomPagination from '../shared/Pagination';
import CustomLoading from '../shared/CustomLoading';

const toastOptions = {
  hideAfter: 5,
  position: 'top-right',
  heading: ''
};

const Users = () => {
  const history = useHistory();
  const [selected, setSelected] = useState(1);

  const { playersGet, players, deletedPlayer, addDeletedPlayer, deletePlayer } =
    usePlayersStore();

  const pageCount = players?.result?.pageCount;

  useEffect(() => {
    playersGet(selected, 10);
  }, [playersGet, selected, deletedPlayer]);
  console.log('players===>', deletedPlayer);
  return (
    <>
      <div className='users_wrapper'>
        <div className='users__heading'>
          <h4 className='font-weight-bold'>
            Ավագանու թեկնածու- {players.countOfCandidateForTheCouncilOfElders}
          </h4>
          <h4 className='font-weight-bold'>
            Համայնքի ակտիվ բնակիչ- {players.countOfActiveResidentOfTheCommunity}
          </h4>
        </div>

        {players?.result ? (
          <CustomTable
            isShownViewIcon={false}
            tableHeaders={[
              'Անուն',
              'Ազգանուն',
              'Հայրանուն',
              'Համայնքի ակտիվ բնակիչ',
              'Ավագանու թեկնածու',
              'Խաղացած խաղեր',
              'Գործողություն'
            ]}
            tableColumns={players?.result?.results}
            renderColumns={(column) => (
              <>
                <td>
                  <div className='d-flex justify-content-center'>
                    <p className='number'>{column.firstName}</p>
                  </div>
                </td>
                <td>
                  <div className='d-flex justify-content-center'>
                    <p className='number'>{column.lastName}</p>
                  </div>
                </td>
                <td>
                  <div className='d-flex justify-content-center'>
                    <p className='number'>{column.middleName}</p>
                  </div>
                </td>
                <td>
                  <div className='d-flex justify-content-center'>
                    {column.scoreActiveResidentOfTheCommunity >= 5 ? (
                      <CheckIcon />
                    ) : (
                      <CloseIcon />
                    )}
                    <p className='number'>
                      {column.scoreActiveResidentOfTheCommunity}
                    </p>
                  </div>
                </td>
                <td>
                  <div className='d-flex justify-content-center'>
                    {column.scoreCandidateForTheCouncilOfElders >= 5 ? (
                      <CheckIcon />
                    ) : (
                      <CloseIcon />
                    )}
                    <p className='number'>
                      {column.scoreCandidateForTheCouncilOfElders}
                    </p>
                  </div>
                </td>
                <td>
                  <div className='d-flex justify-content-center'>
                    <p className='number'>{column.gamesCount}</p>
                  </div>
                </td>
              </>
            )}
            onDeleteClick={(player) => addDeletedPlayer(player._id)}
            onEditClick={(playerId) => {
              playerId.wrongAnswersCount === 0
                ? cogoToast.info(
                    'Տվյալ օգտատերը սխալ պատասխաններ չունի',
                    toastOptions
                  )
                : history.push(`/wrong-answers/${playerId._id}`);
            }}
          />
        ) : (
          <CustomLoading />
        )}

        <CustomPopup
          title='Ջնջել՞'
          isShowed={Boolean(deletedPlayer)}
          onConfirm={() =>
            deletePlayer((currentPage) => setSelected(currentPage))
          }
          onClose={() => addDeletedPlayer(null)}
        />
      </div>
      {pageCount > 1 && (
        <CustomPagination
          count={pageCount}
          current={selected}
          onChange={(value) => setSelected(value)}
        />
      )}
      {/* <SweetAlert
        show={isShowedDeletedModal}
        title="Ջնջել"
        showCancelButton
        confirmButtonText="Այո"
        cancelButtonText="Ոչ"
        onConfirm={() => setShowedDeletedModal(false)}
        customClass="test123123123"
      /> */}
    </>
  );
};

export default Users;
