import React from "react";
import { useQuestionStore } from "../../store/questions";

import questionAddValidation from "../../validations/questionAdd";
import { AppForm, AppFormField } from "../forms";
import { CustomButton } from "../shared";
import CustomDropDown from "../shared/CustomDropDown";
import CustomTextArea from "../shared/CustomTextArea";

const QuestionTemplate = ({ initalValues = {}, language }) => {
  const { questionAdd } = useQuestionStore();

  return (
    <AppForm
      initialValues={{
        ...initalValues,
        language,
      }}
      onSubmit={questionAdd}
      validationSchema={questionAddValidation}
    >
      {(form) => {
        return (
          <>
            <div className="card card__tab">
              <div className="card-body">
                <div className="forms-sample">
                  <CustomDropDown
                    options={["Ավագանու թեկնածու", "Համայնքի ակտիվ բնակիչ"]}
                    name="level"
                    htmlFor="level"
                    wrapperClass="mb-3 question__select"
                    title="Մակարդակ"
                    className="form-control"
                    id="level"
                    handleChange={(option) =>
                      form.setFieldValue("level", option.target.value)
                    }
                  />

                  <CustomTextArea
                    htmlFor="exampleTextarea1"
                    title="Հարց"
                    className="form-control"
                    id="exampleTextarea1"
                    rows="4"
                    name="question"
                  />

                  <div className="row">
                    <div className="col-12 col-md-6 my-3">
                      <AppFormField
                        htmlFor="exampleInputName1"
                        labelTitle="Տարբերակ 1"
                        type="text"
                        className="form-control"
                        id="exampleInputName1"
                        name="question_1"
                      />
                    </div>
                    <div className="col-12 col-md-6 my-3">
                      <AppFormField
                        htmlFor="exampleInputEmail2"
                        labelTitle="Տարբերակ 2"
                        type="text"
                        className="form-control"
                        id="exampleInputName2"
                        name="question_2"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6 my-3">
                      <AppFormField
                        htmlFor="exampleInputEmail3"
                        labelTitle="Տարբերակ 3"
                        type="text"
                        className="form-control"
                        id="exampleInputName3"
                        name="question_3"
                      />
                    </div>
                    <div className="col-12 col-md-6 my-3">
                      <AppFormField
                        htmlFor="exampleInputEmail4"
                        labelTitle="Տարբերակ 4"
                        type="text"
                        className="form-control"
                        id="exampleInputName4"
                        name="question_4"
                      />
                    </div>
                  </div>

                  <CustomDropDown
                    options={["1", "2", "3", "4"]}
                    name="correctAnswer"
                    htmlFor="correctAnswer"
                    wrapperClass="mb-3 question__select"
                    title="Ճիշտ պատասխան"
                    className="form-control"
                    id="correctAnswer"
                    handleChange={(option) =>
                      form.setFieldValue("correctAnswer", option.target.value)
                    }
                  />
                </div>
                <div className="mt-5 text-center">
                  <CustomButton
                    buttonTitle="Ավելացնել"
                    className="btn btn-dark"
                    buttonIcon="fas fa-plus mr-2"
                  />
                </div>
              </div>
            </div>
          </>
        );
      }}
    </AppForm>
  );
};

export default QuestionTemplate;
