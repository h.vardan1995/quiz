import cx from 'classnames';
import React, { useEffect, useState } from 'react';

const UnchangeableInput = ({
  defaultValue,
  disabledText,
  regexp,
  onChange,
  className
}) => {
  const initialValue = defaultValue?.replace(regexp, disabledText);

  const [inputValue, setInputValue] = useState(initialValue);

  useEffect(() => {
    if (initialValue) setInputValue(initialValue);
  }, [initialValue]);

  return (
    <>
      <div
        className={cx('w-100', className)}
        contentEditable
        onInput={(e) => {
          if (!e.target.innerHTML.includes(disabledText)) {
            return (e.target.innerHTML = inputValue);
          }

          onChange(e.target.innerHTML.replace(disabledText, regexp));
          setInputValue(e.target.innerHTML);
        }}
      >
        {initialValue}
      </div>
    </>
  );
};

export default UnchangeableInput;
