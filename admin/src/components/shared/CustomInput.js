import React from "react";
import { Form } from "react-bootstrap";

const CustomInput = ({
  wrapperClassName,
  htmlFor,
  labelTitle,
  id,
  ...otherProps
}) => {
  // console.log(otherProps);
  return (
    <div className={wrapperClassName}>
      <Form.Group>
        <label htmlFor={id}>{labelTitle}</label>
        <Form.Control {...otherProps} id={id} />
      </Form.Group>
    </div>
  );
};

export default CustomInput;
