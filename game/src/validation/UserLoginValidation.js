import * as Yup from 'yup';

const UserLoginValidation = (t) =>
  Yup.object().shape({
    firstName: Yup.string()
      .required(t('validation_text.required'))
      .min(3, t('validation_text.username_login_min'))
      .max(20, t('validation_text.username_login_max'))
      .matches(/^[a-zA-Zա-ևԱ-ֆа-яА-Я]+$/, t('validation_text.name_not_number')),

    lastName: Yup.string()
      .required(t('validation_text.required'))
      .min(3, t('validation_text.username_login_min'))
      .max(20, t('validation_text.username_login_max'))
      .matches(
        /^[a-zA-Zա-ևԱ-ֆа-яА-Я]+$/,
        t('validation_text.surename_not_number')
      ),

    middleName: Yup.string()
      .required(t('validation_text.required'))
      .min(3, t('validation_text.username_login_min'))
      .max(20, t('validation_text.username_login_max'))
      .matches(
        /^[a-zA-Zա-ևԱ-ֆа-яА-Я]+$/,
        t('validation_text.middlename_not_number')
      )
  });

export default UserLoginValidation;
