import * as Yup from 'yup';

const UserRegisterValidation = (t) =>
  Yup.object().shape({
    name: Yup.string()
      .required(t('validation_text.required'))
      .max(30, t('validation_text.register_name_surname_validation')),

    surname: Yup.string()
      .required(t('validation_text.required'))
      .max(30, t('validation_text.register_name_surname_validation')),

    nickName: Yup.string()
      .required(t('validation_text.required'))
      .min(3, t('validation_text.username_login_min'))
      .max(20, t('validation_text.username_login_max')),

    password: Yup.string()
      .required(t('validation_text.required'))
      .min(6, t('validation_text.password_login'))
      .max(20, t('validation_text.password_login'))
      .label()
      .matches(
        /(?:\d+[a-z]|[a-z]+\d)[a-z\d]*/,
        t('validation_text.password_login')
      ),

    passwordRepeat: Yup.string()
      .oneOf([Yup.ref('password'), null], t('validation_text.password_match'))
      .required(t('validation_text.required'))
      .min(6, t('validation_text.password_login'))
      .max(20, t('validation_text.password_login'))
      .label()
      .matches(
        /(?:\d+[a-z]|[a-z]+\d)[a-z\d]*/,
        t('validation_text.password_login')
      )
  });

export default UserRegisterValidation;
