import * as Yup from 'yup';

const ResetPasswordValidation = (t) =>
  Yup.object().shape({
    userName: Yup.string()
      .required(t('validation_text.required'))
      .min(3, t('validation_text.username_login'))
      .max(20, t('validation_text.username_login'))
      .matches(
        /(?:\d+[a-z]|[a-z]+\d)[a-z\d]*/,
        // /^([a-zA-Z0-9]+)$/,
        t('validation_text.username_login')
      )
      .label(),

    oldPassword: Yup.string()
      .required(t('validation_text.required'))
      .min(6, t('validation_text.password_login'))
      .max(20, t('validation_text.password_login'))
      .label()
      .matches(
        /(?:\d+[a-z]|[a-z]+\d)[a-z\d]*/,
        t('validation_text.password_login')
      ),

    newPassword: Yup.string()
      .required(t('validation_text.required'))
      .min(6, t('validation_text.password_login'))
      .max(20, t('validation_text.password_login'))
      .label()
      .matches(
        /(?:\d+[a-z]|[a-z]+\d)[a-z\d]*/,
        t('validation_text.password_login')
      ),

    repeatNewPassword: Yup.string()
      .oneOf(
        [Yup.ref('newPassword'), null],
        t('validation_text.password_match')
      )
      .required(t('validation_text.required'))
      .min(6, t('validation_text.password_login'))
      .max(20, t('validation_text.password_login'))
      .label()
      .matches(
        /(?:\d+[a-z]|[a-z]+\d)[a-z\d]*/,
        t('validation_text.password_login')
      )
  });

export default ResetPasswordValidation;
