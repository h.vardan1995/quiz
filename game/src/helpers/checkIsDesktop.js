export default function checkIsDesktop() {
  return window.innerWidth > 991;
}
