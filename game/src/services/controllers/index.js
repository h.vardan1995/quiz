export { default as authController } from "./user";
export { default as emailController } from "./email";
export { default as gameController } from "./game";
export { default as playerController } from "./player";
