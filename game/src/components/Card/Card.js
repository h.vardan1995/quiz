import React from 'react';
import classes from './Card.module.scss';
import cx from 'classnames';
const Card = ({ children, classStyle }) => {
  return <div className={cx(classes.Card, classStyle)}>{children}</div>;
};
export default React.memo(Card);
