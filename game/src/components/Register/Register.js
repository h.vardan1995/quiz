﻿import React, { useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { Card, Contact, LanguageSelect } from '..';
import { showAlert } from '../../helpers/alerts';
import { Logo } from '../../images/svg';
import { usePlayerStore } from '../../store/player';
import { useUserStore } from '../../store/users';
import UserRegisterValidation from '../../validation/UserRegisterValidation';
import { AppForm, AppFormField } from '../Forms';
import { HelperPerson } from '../HelperPerson';
import classes from '../Register/Register.module.scss';
import { CustomSideNav, Title } from '../shared';
import { CustomButton } from '../shared/CustomButton';

const Register = () => {
  const history = useHistory();
  const { t } = useTranslation();
  const {
    register,
    isShowedInvalidParameters,
    isShowedButtonLoading,
    changeInvalidParameter,
    user
  } = useUserStore((state) => state);

  const { selectedPerson } = usePlayerStore();

  const clearInvalidParameter = useCallback(() => {
    changeInvalidParameter(false);
  }, [changeInvalidParameter]);

  useEffect(() => {
    if (!selectedPerson) history.replace('/');
  }, []);

  return (
    <div className={classes.Register}>
      <Card style={{ marginBottom: '15px' }}>
        <div className={classes.LoginHeader}>
          <div className='d-none d-lg-flex justify-content-end'>
            <LanguageSelect />
            <Contact />
          </div>
        </div>
        <div className={classes.CardHeader}>
          <div className='d-flex d-lg-none justify-content-end align-items-center'>
            {/*<div className={classes.CardHeaderLogo}>
              <Logo />
            </div>*/}
            <CustomSideNav />
          </div>
          <div className={classes.Title}>
            <Title>{t('text.register')}</Title>
          </div>
        </div>

        <AppForm
          initialValues={{
            name: '',
            surname: '',
            nickName: '',
            password: '',
            passwordRepeat: ''
          }}
          onSubmit={(values) =>
            register(
              values,
              () => history.replace('/'),
              () => showAlert('error', t('text.nickName_not_allow'))
            )
          }
          validationSchema={UserRegisterValidation(t)}
        >
          {(form) => {
            return (
              <>
                <div className={classes.CustomInputGroup}>
                  <AppFormField
                    title={t('text.register_name')}
                    placeholder={t('text.register_name')}
                    name='name'
                    onChange={clearInvalidParameter}
                    errorStyle={classes.errorStyle}
                  />

                  <AppFormField
                    title={t('text.register_surname')}
                    placeholder={t('text.register_surname')}
                    name='surname'
                    onChange={clearInvalidParameter}
                    errorStyle={classes.errorStyle}
                  />

                  <AppFormField
                    title={t('text.username')}
                    placeholder={t('text.username')}
                    name='nickName'
                    onChange={clearInvalidParameter}
                    errorStyle={classes.errorStyle}
                  />
                  <AppFormField
                    title={t('text.password')}
                    type='password'
                    placeholder={t('text.password')}
                    name='password'
                    onChange={clearInvalidParameter}
                    errorStyle={classes.errorStyle}
                  />
                  <AppFormField
                    title={t('text.repeatPassword')}
                    type='password'
                    placeholder={t('text.repeatPassword')}
                    name='passwordRepeat'
                    onChange={clearInvalidParameter}
                    errorStyle={classes.errorStyle}
                  />
                </div>
                <div className={classes.Footer}>
                  <div className={classes.Footer_link}>
                    <p>
                      {t('text.haveAccount')}
                      <span onClick={() => history.push('/login')}>
                        {t('text.login')}
                      </span>
                    </p>
                  </div>
                  <div className={classes.ButtonNext}>
                    <CustomButton
                      buttonIcon={isShowedButtonLoading ? 'spinner-border' : ''}
                      title={isShowedButtonLoading ? '' : `${t('text.next')}`}
                    />
                  </div>
                </div>
              </>
            );
          }}
        </AppForm>
        {!user && selectedPerson && (
          <HelperPerson
            isRight
            name='register-helper'
            overlay
            hideAfter={5000}
            personSpeech={t('text.register_now')}
          />
        )}
      </Card>
    </div>
  );
};
export default Register;
