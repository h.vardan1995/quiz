import React, { useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { Card, Contact, LanguageSelect } from '..';
import { showAlert } from '../../helpers/alerts';
import { Logo } from '../../images/svg';
import { usePlayerStore } from '../../store/player';
import { useUserStore } from '../../store/users';
import UserLoginValidation from '../../validation/UserLoginValidation';
import { AppForm, AppFormField } from '../Forms';
import { HelperPerson } from '../HelperPerson';
import { CustomSideNav, Title } from '../shared';
import { CustomButton } from '../shared/CustomButton';
import classes from './Login.module.scss';

const Login = () => {
  const history = useHistory();
  const { t } = useTranslation();

  const {
    user,
    login,
    isLoginInvalidParametrs,
    isLoginButtonLoading,
    changeLoginInvalidParametr
  } = useUserStore((state) => state);

  const { selectedPerson } = usePlayerStore();

  useEffect(() => {
    user
      ? history.replace('/')
      : history.replace(selectedPerson ? '/login' : '/');
  }, [user, history]);

  const clearInvalidParameter = useCallback(() => {
    changeLoginInvalidParametr(false);
  }, [isLoginInvalidParametrs]);

  return (
    <Card>
      <div style={{ overflowY: 'hidden' }}>
        <div className={classes.LoginHeader}>
          <div className='d-none d-lg-flex justify-content-end'>
            <LanguageSelect />
            <Contact />
          </div>
        </div>
        <div className={classes.CardHeader}>
          <div className='d-flex d-lg-none justify-content-end align-items-center'>
           {/* <div className={classes.CardHeaderLogo}>
              <Logo />
            </div>*/}
            <CustomSideNav />
          </div>
        </div>
        <div className={classes.LoginContainer}>
          <div className={classes.LoginWrapper}>
            <Title> {t('text.login')}</Title>
            <AppForm
              initialValues={{
                firstName: '',
                lastName: '',
                middleName: ''
              }}
              onSubmit={(values) =>
                login(values, () =>
                  showAlert('error', t('text.invalid_parametrs'))
                )
              }
              validationSchema={UserLoginValidation(t)}
            >
              {(form) => {
                console.log(form);
                return (
                  <>
                    <div className={classes.CustomInputGroup}>
                      <AppFormField
                        type='text'
                        title={t('text.register_name')}
                        placeholder={t('text.register_name')}
                        name='firstName'
                        onChange={clearInvalidParameter}
                        errorStyle={classes.errorStyle}
                      />
                      <AppFormField
                        title={t('text.register_surname')}
                        type='text'
                        placeholder={t('text.register_surname')}
                        name='lastName'
                        onChange={clearInvalidParameter}
                        errorStyle={classes.errorStyle}
                      />

                      <AppFormField
                        title={t('text.register_middleName')}
                        type='text'
                        placeholder={t('text.register_middleName')}
                        name='middleName'
                        onChange={clearInvalidParameter}
                        errorStyle={classes.errorStyle}
                      />
                    </div>

                    <div className={classes.Footer}>
                      {/* <div className={classes.Footer_link}>
                        <p>
                          {t('text.dontYou')}
                          <span onClick={() => history.push('/register')}>
                            {t('text.register')}
                          </span>
                        </p>
                      </div> */}
                      <div>
                        <CustomButton
                          buttonIcon={
                            isLoginButtonLoading ? 'spinner-border' : ''
                          }
                          title={
                            isLoginButtonLoading ? '' : `${t('text.next')}`
                          }
                        />
                      </div>
                    </div>
                  </>
                );
              }}
            </AppForm>
          </div>
        </div>
      </div>
      {!user && selectedPerson && (
        <HelperPerson
          name='login-helper'
          overlay
          hideAfter={5000}
          personSpeech={t('text.login_now')}
        />
      )}
    </Card>
  );
};
export default React.memo(Login);
