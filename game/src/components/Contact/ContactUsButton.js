﻿import React from 'react';
import { useTranslation } from 'react-i18next';
import classes from './ContactUsButton.module.scss';
import { Phone } from '../../images/svg';

const ContactUsButton = ({ onClick }) => {
  const { t } = useTranslation();

  return (
    <div className={classes.ContactUsButton}>
      <button onClick={onClick}>
        <Phone />
        <span>{t('text.contactWithUs')}</span>
      </button>
    </div>
  );
};

export default React.memo(ContactUsButton);
