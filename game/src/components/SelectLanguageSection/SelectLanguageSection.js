import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { Card, Contact } from '..';
import { checkIsDesktop } from '../../helpers';
import { EnFlag, RuFlag } from '../../images/svg';
import { usePlayerStore } from '../../store/player';
import { useUserStore } from '../../store/users';
import { HelperPerson } from '../HelperPerson';
import { CustomButton, InformationBlock, Language, Title } from '../shared';
import classes from './SelectLanguageSection.module.scss';

const SelectLanguageSection = () => {
  const { t, i18n } = useTranslation();
  const [overlay, setOverlay] = useState(true);
  const [right, setRight] = useState(false);

  const history = useHistory();

  const { setPlayerLang, selectedPerson, selectedLanguage } = usePlayerStore();

  const { user } = useUserStore();

  const changeLanuage = (language) => {
    setPlayerLang(language);
    i18n.changeLanguage(language);
    setOverlay(false);
  };
  const handleOverlay = () => {
    setOverlay(false);
    history.push('/login');
  };
  useEffect(() => {
    if (!selectedPerson && !user) history.replace('/persons');
  }, []);

  return (
    <div className={classes.SelectLanguageSection}>
      <Card>
        <div className={classes.ContactSection}>
          <Contact />
        </div>
        <div className={classes.LanguageSection}>
          <Title>{t('text.selectLang')}</Title>
          <div className={classes.Languages}>
            <div className={classes.LanguagesContainer}>
              <Language
                name='Հայերեն'
                selected={selectedLanguage === 'Am'}
                handleChosseLangiage={() => {
                  changeLanuage('Am');
                }}
                selectedClass={
                  selectedLanguage === 'Am'
                    ? classes.SelectedLanguage
                    : classes.Language
                }
              />
              <Language
                name='English'
                selected={selectedLanguage === 'En'}
                icon={<EnFlag />}
                handleChosseLangiage={() => {
                  changeLanuage('En');
                }}
                selectedClass={
                  selectedLanguage === 'En'
                    ? classes.SelectedLanguage
                    : classes.Language
                }
              />
              <Language
                name='Русский'
                selected={selectedLanguage === 'Ru'}
                icon={<RuFlag />}
                handleChosseLangiage={() => {
                  changeLanuage('Ru');
                }}
                selectedClass={
                  selectedLanguage === 'Ru'
                    ? classes.SelectedLanguage
                    : classes.Language
                }
              />
            </div>

            {/* <Link to='/login'> */}
            <CustomButton
              title={t('text.next')}
              onClick={handleOverlay}
              className={right ? classes.NextButtonRight : ''}
            />
            {/* </Link> */}
          </div>
          <InformationBlock />
        </div>
        {overlay && selectedPerson && (
          <HelperPerson
            name='language-select'
            overlay={overlay}
            isLanguageSelect
            isRight={!checkIsDesktop()}
            personSpeech={t('text.hello')}
          />
        )}
      </Card>
    </div>
  );
};

export default SelectLanguageSection;
