import React from 'react';
import { Link } from 'react-router-dom';
import { Gerb, Logo, Logo2, Logo3 } from '../../images/svg';
import { useGameStore } from '../../store/game';
import { Contact } from '../Contact';
import classes from './Header.module.scss';

const Header = () => {
  const { id } = useGameStore();

  return (
    <div className={classes.Header}>
   {/*   {!id && (
        <div>
          <Link to='/' className={classes.AppLogo}>
            <Logo className='d-none d-lg-block' />
          </Link>
        </div>
      )}*/}
      <div className={classes.LogosContainer}>
        {!id ? (
          <>
            <Logo2 />
            <Gerb />
            <Logo3 />
          </>
        ) : (
          <Contact />
        )}
      </div>
    </div>
  );
};

export default React.memo(Header);
