import cx from 'classnames';
import React, { useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Card, Contact, LanguageSelect } from '..';
import { Logo } from '../../images/svg';
import { useUserStore } from '../../store/users';
import ResetPasswordValidation from '../../validation/ResetPasswordValidation';
import { AppForm, AppFormField } from '../Forms';
import { CustomSideNav, Title } from '../shared';
import { CustomButton } from '../shared/CustomButton';
import classes from './ChangePassword.module.scss';

const ChangePassword = () => {
  const history = useHistory();
  const { t } = useTranslation();

  const {
    isResetInvalidParametrs,
    isResetButtonLoading,
    changeResetInvalidParametr,
    setIsSuccess,
    reset,
    isSuccess
  } = useUserStore();

  const clearInvalidParameter = useCallback(() => {
    changeResetInvalidParametr(false);
  }, [isResetInvalidParametrs]);

  useEffect(() => {
    return () => setIsSuccess(false);
  });

  return (
    <Card>
      <div className={classes.LoginHeader}>
        <div className='d-none d-lg-flex justify-content-end'>
          <LanguageSelect />
          <Contact />
        </div>
      </div>
      <div className={classes.CardHeader}>
        <div className='d-flex d-lg-none justify-content-end align-items-center'>
         {/* <div className={classes.CardHeaderLogo}>
            <Logo />
          </div>*/}
          <CustomSideNav />
        </div>
        <div className={classes.Title}>
          <Title>Փոխել գաղտնաբառը</Title>
        </div>
      </div>

      <AppForm
        initialValues={{
          userName: '',
          oldPassword: '',
          newPassword: '',
          repeatNewPassword: ''
        }}
        onSubmit={(values) => {
          if (isSuccess) return;

          reset(values, () => history.replace('/'));
        }}
        validationSchema={ResetPasswordValidation(t)}
      >
        {(form) => {
          return (
            <>
              <div className={classes.CustomInputGroup}>
                <AppFormField
                  title={t('text.username')}
                  placeholder={t('text.username')}
                  name='userName'
                  onChange={clearInvalidParameter}
                  errorStyle={classes.errorStyle}
                />
                <AppFormField
                  title={t('text.old_password')}
                  type='password'
                  placeholder={t('text.old_password')}
                  name='oldPassword'
                  onChange={clearInvalidParameter}
                  errorStyle={classes.errorStyle}
                />
              </div>
              <div className={cx(classes.CustomInputGroup, 'mt-2')}>
                <AppFormField
                  title={t('text.new_password')}
                  type='password'
                  placeholder={t('text.new_password')}
                  name='newPassword'
                  onChange={clearInvalidParameter}
                  errorStyle={classes.errorStyle}
                />
                <AppFormField
                  title={t('text.repeat_new_password')}
                  type='password'
                  placeholder={t('text.repeat_new_password')}
                  name='repeatNewPassword'
                  onChange={clearInvalidParameter}
                  errorStyle={classes.errorStyle}
                />
              </div>
              <div className={classes.Footer}>
                <div>
                  <CustomButton
                    buttonIcon={isResetButtonLoading ? 'spinner-border' : ''}
                    title={isResetButtonLoading ? '' : `${t('text.change')}`}
                    className='mt-3 pb-3 d-flex justify-content-center'
                  />
                  {isResetInvalidParametrs && (
                    <div className={classes.errorMassage}>
                      Մուտքանունը կամ գաղտնաբառը սխալ են
                    </div>
                  )}

                  {isSuccess && (
                    <div className={classes.successMassage}>
                      Գաղտնաբառը հաջողությամբ փոփոխված է!
                    </div>
                  )}
                </div>
              </div>
            </>
          );
        }}
      </AppForm>
    </Card>
  );
};

export default ChangePassword;
