import React, { useEffect } from 'react';
import classes from '../../shared/InformationBlock/InformationBlock.module.scss';
import { Info } from '../../../images/svg';
import { Paragraph } from '..';

import { useTranslation } from 'react-i18next';

const InformationBlock = () => {
  const { t, i18n } = useTranslation();

  return (
    <div className={classes.InformationBlock}>
      <div>
        <Info />
      </div>
      <div>
        <Paragraph>{t('text.text')}</Paragraph>
      </div>
    </div>
  );
};

export default React.memo(InformationBlock);
