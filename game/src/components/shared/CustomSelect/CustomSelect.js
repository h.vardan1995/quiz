import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import cx from 'classnames';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { ArmFlag, EnFlag, RuFlag } from '../../../images/svg';
import { usePlayerStore } from '../../../store/player';
import { useUserStore } from '../../../store/users';
import classes from './CustomSelect.module.scss';

export default React.memo(function CustomSelect({
  className,
  en = 'En',
  am = 'Հայ',
  ru = 'Ру'
}) {
  const { setPlayerLang, changePlayerLang, selectedLanguage } =
    usePlayerStore();
  const { user } = useUserStore();
  const { i18n } = useTranslation();

  return (
    <div className={cx(classes.CustomSelect, className)}>
      <FormControl>
        <Select
          disableUnderline
          onChange={(event) => {
            i18n.changeLanguage(event.target.value);
            setPlayerLang(event.target.value);
            if (user) changePlayerLang();
          }}
          value={selectedLanguage}
        >
          <MenuItem value={'Am'}>
            <ArmFlag />
            <span className={classes.SelectText}>{am}</span>
          </MenuItem>
          <MenuItem value={'En'}>
            <EnFlag />
            <span className={classes.SelectText}>{en}</span>
          </MenuItem>
          <MenuItem value={'Ru'}>
            <RuFlag />
            <span className={classes.SelectText}>{ru}</span>
          </MenuItem>
        </Select>
      </FormControl>
    </div>
  );
});
