import React from 'react';
import classes from './Typography.module.scss';

export const Title = React.memo(({ children }) => {
  return <h1 className={classes.Title}>{children}</h1>;
});

export const Paragraph = React.memo(({ children }) => {
  return <p className={classes.Paragraph}>{children}</p>;
});
