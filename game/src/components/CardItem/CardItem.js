﻿import React from 'react';
import classes from '../CardItem/CardItem.module.scss';
import cx from 'classnames';

const CardItem = ({ children, handleCheck, error, selected }) => {
  return (
    <div
      className={cx(classes.CardItem, {
        [classes.CardItemSelected]: selected,
        [classes.CardItemError]: error
      })}
      onClick={handleCheck}
    >
      {children}
    </div>
  );
};

export default React.memo(CardItem);
