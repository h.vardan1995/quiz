import React, { useEffect } from 'react';
import classes from './Certification.module.scss';
import { Download, Logo } from '../../images/svg';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { CustomButton } from '../shared';
import { useTranslation } from 'react-i18next';
import cx from 'classnames';
import { useHistory, useParams } from 'react-router-dom';
import { useGameStore } from '../../store/game';
import { usePlayerStore } from '../../store/player';
import { toPng } from 'html-to-image';
import moment from 'moment';
import { Card } from '../Card';
const Certification = ({ match }) => {
  const { t } = useTranslation();
  const history = useHistory();
  const params = useParams();

  const printDocument = () => {
    window.scrollTo(0, 0);

    html2canvas(document.querySelector('#capture'), {
      quality: 1,
      scale: 3,
      allowTaint: true,
      scrollY: 0.25,
      removeContainer: false
    }).then((dataUrl) => {
      const pdf =
        window.innerWidth < 991
          ? jsPDF('l', 'pt', [180, 120])
          : new jsPDF('l', 'pt', [dataUrl.width, dataUrl.height]);

      const width = pdf.internal.pageSize.getWidth();
      const height = pdf.internal.pageSize.getHeight();

      pdf.addImage(dataUrl, 'SVG', 0, 0, width, height);
      pdf.save('Certificate.pdf');
    });
  };

  const { certificateGet, certificate } = useGameStore();
  const userFullname = certificateGet?.fullName.length > 40 ? true : false;
  const userFullname2 = certificateGet?.fullName.length > 19 ? true : false;
  // const userFullname = false;
  useEffect(() => {
    certificate(match.params.gameType);
  }, [userFullname]);

  return (
    <div className={classes.certificationPage}>
      <Card classStyle={window.innerWidth > 991 && classes.card}>
        {window.innerWidth <= 991 && (
          <>
            <Logo />
            <div className={classes.certificateTitle}>Հավաստագիր</div>
          </>
        )}
        <div className={classes.CertificateWrapper} id='capture'>
          <div className={classes.certificateTextWrapper}>
            <p className={userFullname ? classes.smallTitle : classes.title}>
              Հավաստագիր{' '}
            </p>
            <p className={classes.titleEn}>CERTIFICATE OF ACHIEVEMENT</p>
            <p className={classes.presented}>Տրվում է / Presented to</p>
            <p className={userFullname ? classes.smallName : classes.name}>
              {certificateGet?.fullName}
            </p>
            <div className={classes.aboutText}>
              <p className={classes.about}>{certificateGet?.armenianText}</p>
              <p className={classes.aboutEn}>{certificateGet?.englishText}</p>
            </div>
            <div className={classes.signatureWrapper}>
              <div className={classes.signature}></div>
              <p className={classes.date}>
                {moment(certificateGet?.gameComletedDate).format('DD.MM.YYYY')}
              </p>
              <p className={classes.place}>Լոռու մարզ, Ստեփանավան համայնք</p>
            </div>
            {params.gameType === 'ActiveResidentOfTheCommunity' ? (
              <div className={classes.certificationMedal}></div>
            ) : (
              <div
                className={
                  userFullname
                    ? classes.smallCertificationMedal2
                    : classes.certificationMedal2
                }
              ></div>
            )}
          </div>
        </div>
        <div
          className={cx(
            'd-flex',
            'mt-3',
            window.innerWidth <= 991
              ? 'justify-content-center'
              : 'justify-content-end',
            `${classes.buttonWrapper}`
          )}
        >
          <CustomButton
            title={t('text.download')}
            className='mt-3 pb-3 d-flex justify-content-end mr-3'
            onClick={printDocument}
            renderedIcon={<Download className='mr-3' />}
          />
          <CustomButton
            title={t('text.next')}
            onClick={() => history.push('/')}
            className={cx(
              'mt-3',
              'pb-3',
              'd-flex',
              'justify-content-end',
              'mr-2',
              `${classes.nextButton}`
            )}
          />
        </div>
      </Card>
    </div>
  );
};

export default React.memo(Certification);
