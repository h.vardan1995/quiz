import { ErrorMessage, Field } from 'formik';
import React from 'react';
import { CustomInput } from '../shared';
import { default as CustomErrorMessage } from './ErrorMessage';

const AppFormField = ({
  name,
  wrapperClassName,
  renderField,
  errorStyle,
  ...otherProps
}) => {
  return (
    <>
      <Field name={name}>
        {({ field, _, meta }) => {
          return (
            <>
              <div>
                {renderField ? (
                  renderField({
                    ...field,
                    ...otherProps,
                    error: meta.touched && meta.error,
                    onChange: (...args) => {
                      field.onChange(...args);
                      if (otherProps.onChange) otherProps.onChange(...args);
                    }
                  })
                ) : (
                  <>
                    <CustomInput
                      {...field}
                      {...otherProps}
                      onChange={(...args) => {
                        field.onChange(...args);
                        if (otherProps.onChange) otherProps.onChange(...args);
                      }}
                      error={meta.touched && meta.error}
                    />
                    <ErrorMessage
                      name={name}
                      component={CustomErrorMessage}
                      errorStyle={errorStyle}
                    />
                  </>
                )}
              </div>
            </>
          );
        }}
      </Field>
    </>
  );
};

export default React.memo(AppFormField);
