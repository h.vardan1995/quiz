import { createTheme } from '@material-ui/core';

export default createTheme({
  typography: {
    fontFamily: `'Noto-Sans-Armenia', sans-serif`
  }
});
