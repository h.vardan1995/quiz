import create from 'zustand';

import { testApiController } from '../services/controllers/user';

export const useTestApiStore = create((set, get) => ({
  api: [],
  apiGet: async () => {
    try {
      const api = await testApiController.test();

      set({ api });
    } catch (error) {
      console.log(error);
    }
  },
  set
}));
